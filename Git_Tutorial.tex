% Created 2017-08-18 Fri 08:23
% Intended LaTeX compiler: pdflatex
\documentclass[letter,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[margin=0.7in]{geometry}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{amsmath}
\usepackage{setspace}
\singlespacing
\author{James Koch}
\date{\today}
\title{Git Tutorial (and GitHub \ldots{})}
\hypersetup{
 pdfauthor={James Koch},
 pdftitle={Git Tutorial (and GitHub \ldots{})},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 25.2.1 (Org mode 9.0.9)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents

\pagebreak


\section{Foreword}
\label{sec:org6c6ead9}

This document is meant as a guide to core version control concepts by using the Git version control system. It places emphasis on integration with the MATLAB programming language. Once core version control concepts have been explained, this tutorial will emphasize the benefits of sharing code, even text documents with GitHub or a similar service.

\section{Why use version control?}
\label{sec:org58e9a17}

The need for version control can be understood with the following comic strip (Figure \ref{fig-phdcomic})  by Jorge Cham \cite{PHDcomic}.

\begin{figure}[htbp]
\centering
\includegraphics[height=0.5\textwidth]{./WhyUseVersionControl.png}
\caption{Piled Higher and Deeper \label{fig-phdcomic}}
\end{figure}

Version control can eliminate the need to come up with these kinds of file naming conventions which can become quite bizarre. 
Version control allows you to keep \textbf{one} copy of the file and a list of changes that lead to the current state of the file. 
Git is this program that holds all these changes in the background.

However version control does not just stop at eliminating the need for bizarre file naming conventions, it really becomes useful for multiple authors all collaborating on one document in parallel. 
It allows the primary author to manage and accept changes proposed by other authors and furthermore, any previous versions can always be reverted to. 
Finally, version control with a service such as GitHub allows you to backup your data on enterprise systems.

\section{The challenge of Git!}
\label{sec:org734e756}

The challenge with Git is that some command line work is unavoidable as some functionality is only achieved through the command line. The following comic strip (Figure \ref{fig-challengecomic}) shows this challenge \cite{TroubleWithGit}.
With GitHub Desktop some of these barriers are becoming smaller. Nonetheless, keeping a open mind and be willing to search for other tutorials that may explain certain Git concepts differently than here will allow you to get a deeper understanding of Git and version control.

\begin{figure}[htbp]
\centering
\includegraphics[height=0.5\textwidth]{./troublewithgit.png}
\caption{The challenge of Git \label{fig-challengecomic}}
\end{figure}

\section{Sharing \& Git Version Control}
\label{sec:org79180b9}

Git was designed with the main purpose to share information (i.e. files) and services such as GitHub make this very easy to use. In this tutorial, we will use GitHub to begin exploring how to use the Git version control. GitHub has a really excellent guide found at \url{https://guides.github.com/}.

\section{Starting with GitHub}
\label{sec:orgf0bd46b}
\subsection{Starting with Git \& GitHub}
\label{sec:orgdeaa03d}
\begin{enumerate}
\item Learning Objectives
\label{sec:org148bd88}

\begin{itemize}
\item Gain familiarity with GitHub.
\item Begin using version control.
\end{itemize}

\item "Hello World!" GitHub Guide
\label{sec:orgee084f2}

\begin{itemize}
\item We will use the "Hello World!" GitHub Guide. For this you will need to create a account on GitHub. The guide can be found at the following link \url{https://guides.github.com/activities/hello-world/}.
\item We will follow the steps in the GitHub guide together.
\item On completion of the "Hello World!" Guide, we should all be comfortable with creating a repository, creating files, committing files, simple branching, and pull requests.
\end{itemize}
\end{enumerate}

\section{Cloning a GitHub Repository}
\label{sec:org03c5016}
\subsection{Learning Objectives}
\label{sec:org549c2f2}

\begin{itemize}
\item Clone a GitHub repository to your computer.
\end{itemize}

\subsection{Cloning a GitHub Repository}
\label{sec:org4fe9c80}

To clone (i.e. download) a GitHub repository to your local computer, it involves using the following command. Otherwise, using the GitHub Desktop Application will automatically sync all of the repositories within your GitHub account.

\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git clone https://github.com/jchkoch/PoleExtraction
\end{lstlisting}

This command will "clone" the entire contents of the GitHub repository to a folder (directory) on your computer. We will be cloning a GitHub repository of the bridge clearance code which can be found at the following URL: TBD.

\section{Local Git Basics}
\label{sec:org8e5e466}

This section will cover some basic Git version control concepts before extending the idea of version control to multiple authors and sharing of code or text documents. The concepts in this section will be shown using GitHub Desktop and the command line.

\subsection{Learning Objectives}
\label{sec:org60103fb}

\begin{itemize}
\item Explain 3 benefits of using a version control system.
\item Differentiate between various Git file states.
\item Learn to create a local Git repository.
\item Learn how to commit to a local Git repository.
\item Learn how to revert unstaged, staged, and committed changes.
\item Identify why branching is useful.
\end{itemize}

\subsection{Git Version Control States Explanation}
\label{sec:orga266b34}

In Git each file has a status associated with it. This status is concerned with what Git knows about the changes you are making. The life cycle of the status of your files under Git version control is seen in the following Figure \ref{fig-3stages}.

\begin{figure}[htbp]
\centering
\includegraphics[height=0.4\textwidth]{./3stages.png}
\caption{\label{fig:orgc8fe3bb}
Life Cycle Status of Files in Git \label{fig-3stages}}
\end{figure}

First, Git requires the user (you) to \textbf{explicitly} tell it to track (i.e. version) the files in a specific "working" directory. Unless you tell the Git repository to track the files, Git won't track them! Once tracked, Git will "know" about the files but will not yet record a snapshot of (commit) them. 

There exists one step between \textbf{tracking} and \textbf{committing} (i.e. saving a snapshot of your files) which is adding the desired files to the group of changes to be record as the next snapshot (commit). This allows for you to save changes to one file in the repository at different points in time from another file.

The basic Git workflow then becomes:

\begin{enumerate}
\item Modify files in your working directory.
\item Stage the changes you want to record in your next snapshot.
\item Commit the staged changes permanently in the Git repository.
\end{enumerate}

\subsection{Using Git through the Command Prompt}
\label{sec:org1736e88}

To begin using Git, open a command line prompt. This will vary depending on your operating system. If unfamiliar with the command line, see the accompanying command line tutorial. 

For Windows during the installation of Git, a specific program called "Git Bash" was also installed. "Git Bash" is a UNIX style command line emulator to run Git commands. On Mac OSX and Linux, the default command lines can be used. On Mac OSX this is the "Terminal" and on Linux the most common tool is "Bash".

\subsection{Global Git Setup}
\label{sec:orgf7f2d66}

Since Git can be used as a collaborative document version control system, there needs to exist a way to identify changes to a author. This is done by setting a username and email address in your global Git configuration. The following commands will set this up.

\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git config --global user.name "YourName"
git config --global user.email "yourname@example.com"
\end{lstlisting}
\subsection{Basic Git Practice (Command Line)}
\label{sec:org7cdde24}

This section will focus on actually creating and committing to a local Git repository using both the command line and MATLAB.

Once the command line is open, navigate to the home directory using the shell command "cd". Next make a new directory using the "mkdir" command.

To initialize a new Git repository, the following Git command can be used. Since we already have a git repository which we previously cloned from GitHub, this command can be skipped.

\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git init
\end{lstlisting}

\subsection{Basic Git Practice (GitHub Desktop Application)}
\label{sec:orgac5f03f}

If you are using the GitHub Desktop Application, the following can be done to create a new repository.

\begin{enumerate}
\item Start the GitHub Desktop Application. It will most likely ask you to login in with your GitHub username and password. This will enable you to see all of your remote repositories on GitHub.
\item Click the "+" symbol.
\item To create a new repository click "ADD".
\item Navigate and select the folder which contains the files you would like to track.
\item You will be prompted that the folder is \textbf{not yet} a Git repository. Click "create Git repository" and then click "ok".
\item You will see the repository in the left side panel of the desktop application.
\end{enumerate}

\subsection{Git Status Command}
\label{sec:org6a8fbf3}

The basic command to see the status of files in your git repository is shown next. This is not necessary in the Desktop Application as it continually refreshes the contents of each repository automatically. Nonetheless, it is good habit to always be asking yourself when using Git to version control \uline{what have I modified}, \uline{which files have a added to the next snapshot}, and it is also good practice to map out which files do not need to be tracked within a certain repository.

\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git status
\end{lstlisting}

\subsection{Adding \& Committing Changes}
\label{sec:orge0bcc13}

Next add some changes to file called "README". Remember to "save" this file just like you normally would save a file. Git makes the distinction between "saving a file" and "committing  a file"! Once the file is changed, return to the command line or desktop application and let's stage and commit the changes as a new snapshot. The following commands on the command line can do this otherwise on the desktop application simply clicking on the repository in the left side panel will come up with a display of all the files ready to be committed by entering a commit message and clicking enter. Note by using the Desktop Application, you do not need to "stage" files to your staging area (this is done by the application behind the scenes).

\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git add .
git status
git commit -m "modified README file"
\end{lstlisting}

\begin{enumerate}
\item Is the \label{org752fe0d} necessary?
\begin{itemize}
\item Strictly speaking it is \textbf{not} necessary; however, it is good practice to always check what the state of the files in your working directory are in.
\end{itemize}
\item What does the \label{orgb682a41} do in the first line of code?
\begin{itemize}
\item It stages all modified and untracked files.
\end{itemize}
\item Why add a commit message as in line three?
\begin{itemize}
\item To provide a quick, short message regarding what changes occur in the commit all in one line. Otherwise, a editor (most likely Vim) will open in the command line for you to type a commit message. (Even if you are using the GitHub Desktop Application, think about the ideas behind these questions as they provide a deeper insight into the power of Git version control.)
\end{itemize}
\end{enumerate}

\subsection{View Version History}
\label{sec:orgfac6194}

The next command this tutorial will teach you to use on the command line or the desktop application is to view the history of commits. This is useful to be able to see the history of changes and be able to identify a specific time in the history if you want to go back to a previous version. The following is the command to use on the command line. The desktop application has a visual depiction of the history on the top panel where if you click on any of the nodes, you will see more information related to the commit.

\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
# Basic command to display commit history
git log
# Easier visual depiction of git commit history
git log --graph --oneline --abbrev-commit --decorate --date=relative --all 
\end{lstlisting}

If the second command seems complicated, you can either ignore it and come back to it once you are more comfortable with Git or you can confidently use it with the knowledge that you \textbf{cannot} harm your work with this command.

\subsection{Checkout and Revert Commands (Advanced Topics)}
\label{sec:org040796d}

The final two commands in this tutorial will let you view any state of your project at any point from your history and revert back to a previous snapshot. The first command to view any previous state of your project is useful to figure out whether or not to back up to a previous snapshot (i.e. go in a different direction with your project). The second command actually allows you to go back to a previous snapshot. It does this by creating a new snapshot which undoes the changes of a certain commit in the history. This is important because it allows Git to \textbf{never delete} any changes thereby allowing you to maintain the integrity of the repository history and data. Additionally, \label{org4233c35} can target a single commit at any point in the history and undo those specific changes.

If the above explanation is more confusing than explanatory, don't worry starting out as you can use git without using these commands and still be successful. Git can be used wonderfully as a backup system (if you need to backup and are unsure, there are plenty of resources and forums that you can turn to for help).

\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git checkout <commit>
git revert <commit>
\end{lstlisting}


\bibliographystyle{plain}
\bibliography{references}
\end{document}